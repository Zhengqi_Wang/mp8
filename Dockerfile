FROM rust:latest as builder

WORKDIR /usr/src/my_rust_cli
COPY Cargo.toml Cargo.lock ./
# Create a dummy main.rs to build dependencies
RUN mkdir src/ && echo "fn main() {}" > src/main.rs
RUN cargo build --release
# Remove the dummy main.rs and copy the actual source code
RUN rm src/*.rs
COPY . .
RUN cargo build --release

# The final image will be based on Debian
FROM debian:buster-slim
COPY --from=builder /my_rust_cli/target/release/my_rust_cli .

# Set the binary as the entrypoint of the container
ENTRYPOINT ["./my_rust_cli"]
