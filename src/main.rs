use clap::{App, Arg};
use csv::Reader;
use std::error::Error;

fn read_csv_data(file_path: &str) -> Result<Vec<f64>, Box<dyn Error>> {
    let mut rdr = Reader::from_path(file_path)?;
    let mut data = Vec::new();

    for result in rdr.records() {
        let record = result?;
        if let Some(value) = record.get(0) {
            data.push(value.parse::<f64>()?);
        }
    }

    Ok(data)
}

fn calculate_moving_average(data: &[f64], window_size: usize) -> Vec<f64> {
    let mut moving_averages = Vec::new();

    for window in data.windows(window_size) {
        let sum: f64 = window.iter().sum();
        moving_averages.push(sum / window.len() as f64);
    }

    moving_averages
}

fn main() {
    let file_path = "/Users/supergeorge/Documents/IDS721_HW8/dataFile.csv"; 
    let window_size = 3; // Customize your window size for the moving average

    match read_csv_data(file_path) {
        Ok(data) => {
            let moving_averages = calculate_moving_average(&data, window_size);
            println!("Moving Averages: {:?}", moving_averages);
        },
        Err(e) => eprintln!("Error reading CSV data: {}", e),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calculate_moving_average() {
        let data = vec![10.0, 20.0, 30.0, 40.0, 50.0];
        let window_size = 3;
        let moving_averages = calculate_moving_average(&data, window_size);

        assert_eq!(moving_averages, vec![20.0, 30.0, 40.0]);

        // Test with a different window size
        let window_size = 2;
        let moving_averages = calculate_moving_average(&data, window_size);

        assert_eq!(moving_averages, vec![15.0, 25.0, 35.0, 45.0]);
    }

}
