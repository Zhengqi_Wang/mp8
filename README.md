# Moving Average CLI Tool in Rust

This project demonstrates a Rust-based command-line tool that calculates the moving average from a dataset provided in a CSV file. Users can specify the window size for the moving average calculation.

## Project Structure

- `src/main.rs`: Contains the main logic for the CLI tool, including argument parsing and data processing.\
    Step 1: Read and Store CSV Data
    Read the data from the CSV file and store it in a vector. Since our CSV contains only one column of numerical data, we can simplify the ingestion process.
    ```
    use csv::Reader;
    use std::error::Error;

    fn read_csv_data(file_path: &str) -> Result<Vec<f64>, Box<dyn Error>> {
        let mut rdr = Reader::from_path(file_path)?;
        let mut data = Vec::new();

        for result in rdr.records() {
            let record = result?;
            if let Some(value) = record.get(0) {
                data.push(value.parse::<f64>()?);
            }
        }

        Ok(data)
    }
    ```
    Step 2: Implement the Moving Average Function
    The moving average function will take the data and the window size as arguments and return a vector of the moving averages.
    ```
    fn calculate_moving_average(data: &[f64], window_size: usize) -> Vec<f64> {
        let mut moving_averages = Vec::new();

        for window in data.windows(window_size) {
            let sum: f64 = window.iter().sum();
            moving_averages.push(sum / window.len() as f64);
        }

        moving_averages
    }
    ```
    Step 3: Integrate with Main Function
    Integrate these functions in main function to read the data and calculate the moving average.
    ```
    fn main() {
        let file_path = "data.csv"; // Replace with your CSV file path
        let window_size = 3; // Customize your window size for the moving average

        match read_csv_data(file_path) {
            Ok(data) => {
                let moving_averages = calculate_moving_average(&data, window_size);
                println!("Moving Averages: {:?}", moving_averages);
            },
            Err(e) => eprintln!("Error reading CSV data: {}", e),
        }
    }
    ```
    Step 4: Error Handling and Improvements
    Ensure proper error handling in your code. In the examples above, the '?' operator is used for error propagation, and any issues with reading the data or parsing numbers will result in an early return with an error.
- `Cargo.toml`: Manages Rust package information and dependencies.
    Ensure that your Cargo.toml file specifies the correct versions of your dependencies. 
    ```
    [dependencies]
    clap = "3.0"
    csv = "1.1"
    ```
- `data.csv`: An example CSV file containing the data for which the moving average is calculated (not included in the repo, to be supplied by the user).

## How to Run the Project
1. **Local Testing**:
    Run `cargo test` for testing
    ![cargo test](./Images/cargo_test.png)
2. **Build the Project**:
    Run `cargo run` to compile
    ![cargo](./Images/cargo_run.png)
    Run `cargo build --release` to compile the project in release mode.
    ![cargo build --release](./Images/cargo_run_release.png)
3. **Run the Tool**:
   Execute the tool using `cargo run --release -- dataFile.csv window_size`, where `window_size` is an integer defining the range of the moving average.
   ![cargo](./Images/cargo_run_release_6.png)

## Features

- Command-line argument parsing using `clap`.
- CSV file reading and processing.
- Calculation of the moving average with a user-defined window size.

## Setting Up and Running

### Prerequisites

- Rust programming environment (rustc, cargo)

### Instructions

1. Clone the repository and navigate to the project directory.
2. Build the project: `cargo build --release`.
3. Run the tool: `cargo run --release -- your_file_path.csv window_size`.